﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NHS_Task1_C_sharp
{
    public class PostCodeValidateHelper
    {
        public bool ValidatePostCode(string postCode)
        {
            // Added ^ sign at the begining and added $ sign to the provided regular expression to match the whole postcode. 
            // This regular expression matches all the uk postalcodes.
            Regex regex = new Regex(@"^((GIR\s0AA)|((([A-PR-UWYZ][0-9][0-9]?)|(([A-PR-UWYZ][A-HK-Y][0-9](?<!(BR|FY|HA|HD|HG|HR|HS|HX|JE|LD|SM|SR|WC|WN|ZE)[0-9])[0-9])|([A-PR-UWYZ][A-HK-Y](?<!AB|LL|SO)[0-9])|(WC[0-9][A-Z])|(([A-PR-UWYZ][0-9][A-HJKPSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))))\s[0-9][ABD-HJLNP-UW-Z]{2}))$");

            Match match = regex.Match(postCode);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
