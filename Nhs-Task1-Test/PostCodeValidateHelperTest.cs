﻿using NHS_Task1_C_sharp;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nhs_Task1_Test
{
    [TestFixture]
    public class PostCodeValidateHelperTest
    {
        [Test]
        public void ShouldNotMatchPostCodeEC1A_1BB()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("EC1A 1BB");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeW1A_0AX()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("W1A 0AX");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeM1_1AE()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("M1 1AE");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeB33_8TH()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("B33 8TH");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeCR2_6XH()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("CR2 6XH");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeDN55_1PT()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("DN55 1PT");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeGIR_0AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("GIR 0AA");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeSO10_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("SO10 9AA");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeFY9_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("FY9 9AA");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchPostCodeWC1A_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("WC1A 9AA");

            Assert.That(result, Is.True);
        }

        [Test]
        public void ShouldNotMatchJunkPostCode()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("$%± ()()");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeXX_XXX()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("XX XXX");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeA1_9A()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("A1 9A");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeLS44PL()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("LS44PL");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeQ1A_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("Q1A 9AA");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeV1A_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("V1A 9AA");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeX1A_9BB()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("X1A 9BB");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeLI10_3QP()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("LI10 3QP");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeLJ10_3QP()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("LJ10 3QP");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeLZ10_3QP()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("LZ10 3QP");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeA9Q_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("A9Q 9AA");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeAA9C_9AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("AA9C 9AA");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeFY10_4PL()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("FY10 4PL");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldNotMatchPostCodeSO1_4QQ()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("SO1 4QQ");

            Assert.That(result, Is.False);
        }

        [Test]
        public void ShouldMatchPostCodeE2_1AA()
        {
            PostCodeValidateHelper suts = new PostCodeValidateHelper();
            var result = suts.ValidatePostCode("E2 1AA");

            Assert.That(result, Is.True);
        }
    }
}
